# Toby And The Strange Function

Ejercicio de la maratón de programación, modificado para entrega de proyecto 
final de teoría de la computación.

## Problema

[Descargar pdf](toby-strange-function.pdf).

## Explicación

Vamos a suponer el siguiente caso:

La funcion f(n, s), donde n = 4 y s = "abcd".

El algoritmo deberá ser capaz para cada valor de n, tomar el último caracter
de la cadena y ponerlo en la parte de adelante de la misma. Ejemplo:

f(1, "abcd") = "dabc"

f(2, "abcd") = "cdab"

f(3, "abcd") = "bcda"

f(4, "abcd") = "abcd" --> n % s.length == 0.

Existen un patrón en este ejemplo y es que cada vez que n % s.length == 0, 
siempre dará la cadena inicial:

f(5, "abcd") = "dabc"

f(6, "abcd") = "cdab"

f(7, "abcd") = "bcda"

f(8, "abcd") = "abcd" --> n % s.length == 0.

Cuando el tamaño de la cadena es divisor de n (su resíduo da cero), entonces
siempre dará la misma cadena inicial, de lo contrario, se toma el último caracter
de la cadena y se pone al inicio de ella. Luego, se llama la función de manera
recursiva con n-1: f(n-1, s).