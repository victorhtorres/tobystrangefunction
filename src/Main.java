
import java.util.regex.Pattern;

public class Main {
    
    public static void main(String[] args) {

        //El peor caso. Cuando n = (10^18)-1 y se evalua una cadena de 100 caracteres.
        long n = (long) (Math.pow(10, 18))-1;
        String cad = "abcdefghijklnmñopqrstuvwxyzabcdefghijklnmñopqrstuvwxyzabcdefghijklnmñopqrstuvwxyzabcdefghijklnmñopqr";
        System.out.println("n = " + n);
        System.out.println("S = " + cad.length());


        // validar las entradas según las restricciones del problema
        boolean cadenaValida = validarCadena(cad);
        boolean enteroValido = validarN(n);
        int tamanoCadena = cad.length();

        if (cadenaValida && enteroValido && tamanoCadena <= 100) {
            System.out.println(strangeFunction(n, cad));
        } else {
            if (!cadenaValida) {
                System.out.println("Error. La cadena ingresada no es válida. Sólo se permite caracteres en minusculas de la a a la z.");
            }

            if (!enteroValido) {
                System.out.println("Error. Ha ingresado un valor entero que no cumple con la restricción n(0 <= n <= 10^18)");
            }

            if (tamanoCadena > 100) {
                System.out.println("Error. El tamaño de la cadena excede los 100 caracteres.");
            }
        }

    }

    /**
     * Método recursivo que coloca el último caracter de la cadena al inicio
     * de ella
     * @param n El número de veces que tomará el último caracter y lo pondrá al
     * inicio.
     * @param cad La cadena a modificar.
     * @return String - Cadena modificada.
     */
    private static String strangeFunction(long n, String cad) {
        if (n % cad.length() == 0) {
            return cad;

        } else {
            String ultimo = cad.substring(cad.length() - 1);
            String subCadena = cad.substring(0, cad.length() - 1);
            cad = ultimo + subCadena;
            return strangeFunction(n - 1, cad);
        }

    }
    
    /**
     * Método que valida que la cadena ingresada sea sólo caracteres en
     * minusculas, incluyendo la ñ
     * @param cad - Cadena a evaluar.
     * @return boolean
     */
    private static boolean validarCadena(String cad) {
        boolean resultado = Pattern.matches("[a-z||[ñ]]+", cad);
        return resultado;
    }

    /**
     * Método que valida que el número entero n
     * @param n
     * @return boolean
     */
    private static boolean validarN(long n) {
        if (0 <= n && n <= (long) Math.pow(10, 18)) {
            return true;
        } else {
            return false;
        }
    }

}
